module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    clean: {
      local: {
        src: ['temp/local']
      },
      prod: {
        src: ['temp/prod', 'dist/prod']
      }
    },

    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['app/scripts/**/*.js'],
        dest: 'dist/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    qunit: {
      files: ['test/**/*.html']
    },
    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    copy: {
      local: {
        files: [
          {dest: 'dist/local/index.html', src:['index.html']},
          {dest: 'temp/local/scripts/', src: ['**'], cwd: 'app/scripts/', expand: true},
          {dest : 'temp/local/templates/', src : ['**'], cwd : 'app/templates/', expand : true },

          {dest: 'temp/local/assets/', src: ['**'], cwd: 'app/assets/', expand: true},
          {dest: 'dist/local/assets/', src: ['**'], cwd: 'app/assets/', expand: true}
        ]
      },
      prod: {
        files: [
          {dest: 'dist/prod/index.html', src:['dist-index.html']},
          {dest: 'temp/prod/scripts/', src: ['**'], cwd: 'app/scripts/', expand: true},
          {dest : 'temp/prod/templates/', src : ['**'], cwd : 'app/templates/', expand : true },

          {dest: 'temp/prod/assets/', src: ['**'], cwd: 'app/assets/', expand: true},
          {dest: 'dist/prod/assets/', src: ['**'], cwd: 'app/assets/', expand: true}
        ]
      }
    },
    watch: {
      files: ['app/**'],
      tasks: ['compile:local']
    },

    //Compile
    less: {
        local: {
            options: {
                paths: ['app/styles'],
                filename: 'style.less'
            },
            files: [
                { dest : 'temp/local/styles/<%= pkg.name %>.css', src : 'app/styles/start.less' }
            ]
        },

        prod: {
            options: {
                paths: ['app/styles'],
                compress: true
            },
            files: [
                { dest : 'dist/prod/styles/<%= pkg.name %>.css', src : 'app/styles/start.less' }
            ]
        }
    },

    // Dist
    requirejs: {
      local: {
        options: {
          // Need to debug the release code? Uncomment the optimize flag
          // to get a readable javascript output
          // optimize: "none"
          baseUrl       : 'temp/local/scripts',
          mainConfigFile: 'app/scripts/config.js',
          name          : '../../../bower_components/almond/almond',
          include       : 'app',
          insertRequire : ['app'],
          out           : 'dist/local/scripts/<%= pkg.name %>.js',
          wrap          : true,
          paths         :{
            env         : 'environment/local'
          }
        }
      },
      prod: {
        options: {
          // Need to debug the release code? Uncomment the optimize flag
          // to get a readable javascript output
          // optimize: "none"
          baseUrl       : 'temp/prod/scripts',
          mainConfigFile: 'app/scripts/config.js',
          name          : '../../../bower_components/almond/almond',
          include       : 'app',
          insertRequire : ['app'],
          out           : 'dist/prod/scripts/<%= pkg.name %>.js',
          wrap          : true,
          paths         :{
            env         : 'environment/prod'
          }
        }
      }
    },
    // Hashing of resources
    hashres: {
        prod: {
            src: ['dist/prod/scripts/*.js', 'dist/prod/styles/*.css'],
            dest: 'dist/prod/index.html'
        }
    },

    //Generate the preloader manifest
    // preloadManifest: {
    //     data: {
    //         srcDir: 'app/',
    //         assetsDir: 'app/assets/',
    //         overwriteManifest: true
    //     }
    // },


    //strip all console functions
    strip: {
        prod: {
            src: 'dist/prod/scripts/*.js',
            options: {
                inline: true
            }
        }
    },

    compress: {
        sim: {
            options: {
                archive: 'dist/<%= pkg.name %>.zip'
            },
            files: [
                {cwd: 'dist/prod/', src: ['**/*'], expand: true}
            ]
        }
    }
  });

  grunt.loadNpmTasks('grunt-contrib');
  grunt.loadNpmTasks('grunt-preloader-manifest-generator');
  grunt.loadNpmTasks('grunt-strip');
  grunt.loadNpmTasks('grunt-hashres');

  grunt.registerTask('test', ['jshint', 'qunit']);

  grunt.registerTask('default', ['jshint', 'qunit']);
  grunt.registerTask('compile:local', ['clean:local','less:local', 'copy:local', 'test']);
  grunt.registerTask('compile:prod', ['clean:prod', 'less:prod','copy:prod', 'test', 'requirejs:prod', 'hashres:prod', 'strip:prod', 'compress']); //, 'strip:prod'
};