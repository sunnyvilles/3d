/*globals Image,document*/
define(function(require){
    
    //require('jqueryCsv');
    var _ = require('underscore'),
     Backbone = require('backbone'),
     $ = require('jquery'),
     _ = require('underscore'),

     //env = require('env'),
     elementTemplate = require('text!../../templates/element.tpl'),
     d3    = require("d3"),

     jsonMapStr = require('text!../../assets/map.json'),
    
     elementView = Backbone.View.extend({

        className: 'elementView',

        initialize: function(params){

            _.bindAll(this);

            //i is count number of element
            
            this.jsonMap = JSON.parse(jsonMapStr);
            params = params || {};
            
            var compiledTemplate = this.dataMapping(params);
            this.$el.html(compiledTemplate);

            this.rgbColors = {
                green : "73,226,14",//"222,239,52"
                greenBorder:"222,239,52",
                grey : "211,211,211",//"92,71,52",//"103,68,40"
                greyBorder:"92,71,52",
                red : "255,69,0",//"234,15,114"
                redBorder:"255,0,0"
                //674428, 5C4734, DEEF34
            };

            this.elementColor = this.getRGBElement(params.data["Relationship NPS Score"]); ;
            this.styling(params.ele.parentElement);
        },
        
        getRGBElement:function(npsVal){

            if(!npsVal){
                npsVal = 5;
            }
            var col;

            if(npsVal<7){
                col = "red";
            }else if(npsVal<9){
                col = "grey";
            }else if(npsVal>8){
                col = "green";
            }

            return col;

        },
        
        styling:function(el){
            
            var that = this;

            //lets hardcode :D
            this.elementColor = "green";

            $(el)
                    .css({
                       'background-color' : 'rgba('+that.rgbColors[that.elementColor]+',.75)',
                       'border' : '2px solid rgba('+that.rgbColors[(that.elementColor+'Border')]+',0.25)'
                    });

            //text color 


            if(this.elementColor === 'red'){
                $(el)
                    .css({
                        'color': 'white'
                    });
            }

            $(el).hover(function(){ // on hover

                $(this)
                    .css({
                       'box-shadow' : '0px 0px 12px rgba(0,255,255,0.75)',
                       'border' : '1px solid rgba('+that.rgbColors[(that.elementColor+'Border')]+',0.75)'
                    });

            },function(){ // on remove hover

                $(this)
                    .css({
                       'box-shadow' : '',
                       'border' : '2px solid rgba('+that.rgbColors[(that.elementColor+'Border')]+',0.25)'
                    });
            })
        },

        dataMapping: function(params){

            var that = this;
            for (var key in that.jsonMap) {

                var k = that.jsonMap[key],
                v = params.data[that.jsonMap[key]] ;

                that.jsonMap[key] = {
                    key  : k, 
                    value: v
                };
            }

 
            this.jsonMap.i = params.i;

            for(var i = 1; i < 13;i++){
                var k = "key" + i;
                if(!this.jsonMap[k]){
                    this.jsonMap[k] = " ";
                }
            }

            //var data = {data: };
            var compiledTemplate = _.template(elementTemplate, this.jsonMap);

            return compiledTemplate;
        }


    });

    return elementView;
 });