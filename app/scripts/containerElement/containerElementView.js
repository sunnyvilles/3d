/*globals window,console,document*/
define(function(require){

    var _           = require('underscore');
    var $           = require('jquery');
    var Backbone    = require('backbone');
    var template = require('text!../../templates/containerElement.tpl');
    //var ContainerElement = require('containerElement/containerElement');
    var ElementSystemView = require('elementSystem/elementSystemView');

    var THREE    = require("threejs");
    require("tweenJs");
    require("TrackballControls");
    require("CSS3DRenderer");
    var d3    = require("d3");

    var width = window.innerWidth, height = window.innerHeight;

    var containerElementView = Backbone.View.extend({

        className   : 'containerElementView',

        // events  : {
        //     'mousemove document'   : 'mouseOvered'
        // },
        initialize: function (params) {
            
            _.bindAll(this);
            this.$el.html(template);

            this.$("#loading").remove();

            this.scene = new THREE.Scene();
            this.renderer = new THREE.CSS3DRenderer();
            this.camera = new THREE.PerspectiveCamera(40, width/height , 1, 10000);

            this.elementsArray = [];
            this.objectifiedElements = [];

            window.objs = this.objectifiedElements || {};
        },

        initRenderer : function(){

            this.renderer.setSize(width, height);
            this.renderer.domElement.style.position = 'absolute';
            
            this.$('#elementsContainer').get(0).appendChild(this.renderer.domElement);

            this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);
        },

        initElementSystem: function(){

            var settings3D = {
                scene : this.scene,
                camera: this.camera,
                renderer:this.renderer,
                tweenJs:TWEEN,
                objectifiedElements:this.objectifiedElements,
                elementsArray: this.elementsArray
            };

            this.elementSystemView = new ElementSystemView(settings3D);
        },

        initCamera : function(){
            this.camera.position.z = 3000;
            this.camera.setLens(30);
            this.camera.up.set(0, 1, 0);
        },

        initControls: function(){

            this.controls.rotateSpeed = 0.5;
            this.controls.minDistance = 100;
            this.controls.maxDistance = 6000;
            this.controls.addEventListener('change',this.elementSystemView.render);

        },

        initTransition : function(jsonData){

            this.initRenderer();
            this.initElementSystem();

            this.initCamera();
            this.initControls();

            var elements = this.elementSystemView.createElements(jsonData),
            that = this;

            elements.each(this.elementSystemView.setData);

            //objectify to css3dObjects
            elements.each(function(d) {
                
                var object = new THREE.CSS3DObject(this);
                object.position = d.random.position;

                var loc = that.elementsArray.push(this);
                that.objectifiedElements[loc-1] = object;
                that.scene.add(object);

            });

            this.elementSystemView.transform('grid');
            this.elementSystemView.render();
            this.animate();
            window.addEventListener('resize', this.onWindowResize, false);
        },

        animate : function () {
          requestAnimationFrame(this.animate);
          TWEEN.update();
          this.controls.update();
        },

        onWindowResize : function () {
            this.camera.aspect = width / height;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize(window.innerWidth, window.innerHeight);
            this.elementSystemView.render();
        }
    })

    return containerElementView;

});