/*global requirejs, THREE */
requirejs.config({
  shim: {
    jquery: {
      exports: '$'
    },
    underscore:{
        exports: '_'
    },
    backbone:{
        deps    : ['jquery', 'underscore'],
        exports : 'Backbone'
    },
    d3:{
        deps    : ['jquery', 'underscore'],
        exports : 'd3'
    },
    'touch-punch': {
       deps   : ['jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse'],
       exports : '$'
    },
    threejs: {
        //deps: ['polyfill-typedarray'],
        exports : 'THREE',
        init: function(){
            return THREE;
        }
    },
    TrackballControls:{
      deps    : ['threejs']
    },
    tweenJs: {
       exports : 'TWEEN'
        // init: function(){
        //     return createjs;
        // }
    },
    CSS3DRenderer:{
      deps    : ['threejs','TrackballControls']
    }

    // jqueryCsv: {
    //   deps    : ['jquery'],
    // }

  },
  paths: {
    //env         : 'environment/local',
    text        : '../../../bower_components/requirejs-text/text',
    jquery      : '../../../bower_components/jquery/jquery',
    underscore  : '../../../bower_components/underscore/underscore',
    d3          : '../../../bower_components/d3/d3.min',    
    backbone    : '../../../bower_components/backbone/backbone',
    'touch-punch':"../../../bower_components/jqueryui-touch-punch/jquery.ui.touch-punch",
    'threejs'     : "../../../bower_components/threejs/build/three",
    'TrackballControls':"../../../common-libs/TrackballControls-custom",
    'CSS3DRenderer'     : "../../../common-libs/CSS3DRenderer",
    'tweenJs'     : "../../../common-libs/tween"
    //'jqueryCsv'  :"../../../common-libs/jquery.csv.min"
    //'polyfill-typedarray'    : "../../../bower_components/polyfill/typedarray"
  }
});