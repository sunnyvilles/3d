/*globals console*/
define (function(require){

    var $ = require('jquery');
    var Backbone = require('backbone');

    //var notSupportedTemplate = require('text!../templates/notSupported.html');

    // if (!checkBrowser()) {
    //     $('body').prepend(notSupportedTemplate);
    //     return;
    // }
    
    var template = require('text!../templates/mainContainer.tpl');
    var ContainerElementView = require('containerElement/containerElementView');

    //get local test data
    //var jsonDataStr = require('text!../assets/investments.csv');
    //var jsonData = JSON.parse(jsonDataStr);
    //var jsonData = $.csv.toObjects(jsonDataStr);

    var jsonDataStr = require('text!../assets/investments.json');
    var jsonData = JSON.parse(jsonDataStr);

    function init(){



        //this.renderer = new THREE.CSS3DRenderer();

        $('body').append(template);

        //$('#initialLoadImageDiv').remove();

        // Initialise App view
        this.containerElementView = new ContainerElementView();

        $('.background').html(this.containerElementView.$el);

        this.containerElementView.initTransition(jsonData);
        //$('.background').html(this.containerElementView.$el);

    }

        //shim request animation frame with setTimeout fallback (for IE9)
    window.requestAnimationFrame =
        window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function( callback ){
            window.setTimeout( callback , 1000 / 60 );
        };

        $(document).ready( init );

});