/*globals window,console,document*/
define(function(require){

    var _ = require('underscore'),
     Backbone = require('backbone'),
     $ = require('jquery'),
     //env = require('env'),

     template = require('text!../../templates/elementSystem.tpl'),
     THREE    = require("threejs"),
     d3    = require("d3"),
     TWEEN;

     var ElementView = require('element/elementView');
     var elementSystemView = Backbone.View.extend({

        className: 'elementSystemView',

        initialize: function(params){

            _.bindAll(this);
            this.$el.html(template);

            params = params || {};

            // Parameters that the view must receive
            this.scene = params.scene !== undefined ? params.scene : new THREE.Scene();
            this.camera = params.camera ;
            this.renderer = params.renderer;
            TWEEN = params.tweenJs;
            
            this.objectifiedElements = params.objectifiedElements;
            this.elementsArray = params.elementsArray;
 
            this.zoomed = false;
            this.zoomedElement = null;

            this.NOfRow = 3;
            this.NOfColumn = 7;
            this.NOFElementsInAPlane = this.NOfRow*this.NOfColumn;

            this.createMoveButtons();

           // this.cameraStable = false;
            this.camQuaternion = $.extend({}, this.camera.quaternion);

            this.count = 0;
            this.totalPlanes = 0;
            this.zExpansionFromOrigin = 0;

            this.distBetPlanes = 1000;
            this.distBetElements = 400;

        },

        createMoveButtons: function(){
          var that = this;
          d3.select("#menu").selectAll('button')
              .data(['back', 'forward']).enter()
                .append('button')
                .html(function (d) { return d; })
                .on('click', function (d) { that.moveGrids(d); })
        },

        //logic for creating the elements
        createElements: function(data){

            var that = this;
            this.count = data.length;

            this.totalPlanes = Math.ceil( this.count/this.NOFElementsInAPlane);
            
            this.zExpansionFromOrigin = (this.totalPlanes-1) * this.distBetPlanes/2 ;
            this.yExpansionFromOrigin = (this.NOfRow-1) * this.distBetElements/2 ;
            this.xExpansionFromOrigin = (this.NOfColumn-1) * this.distBetElements/2 ;

            var elements = d3.select('#elementsContainer').selectAll('div.element')
                .data(data).enter()
                .append('div')
                .attr('class', function(d,i){
                  console.log(i);
                  var planeNo = Math.ceil( (i+1) / that.NOFElementsInAPlane ) ;
                  return ('element elementPlane' + planeNo);
                });

            elements.append('div')
              .attr('class', 'elementData')
              .html(function (d,i) {
     
                  var param = {data:d,i:i,ele:this};
                  var elementView = new ElementView(param);

                  return elementView.$el.html(); 
    
              });

            elements
              .on("click", function() {
                var obj = that.objectifiedElements[that.elementsArray.indexOf(this)];
                if(that.zoomed){
                  //if not clicked on same object
                  if(that.zoomedElement.element !== obj){
                    that.restoreMovedElement();
                    that.zoomElement(obj);
                  }else if(that.zoomedElement.element === obj){
                    that.restoreMovedElement();
                  }
                }else if(!that.zoomed){
                  that.zoomElement(obj);
                }

              });

            return elements;
        
        },

        setData: function(d, i) {

          var random = new THREE.Object3D();
          random.position.x = Math.random() * 4000 - 2000;
          random.position.y = Math.random() * 4000 - 2000;
          random.position.z = Math.random() * 4000 - 2000;
          d['random'] = random;

          var grid = new THREE.Object3D();
          grid.position.x = (( i % this.NOfColumn ) * 400) - this.xExpansionFromOrigin;
          grid.position.y = ( - ( Math.floor( i / this.NOfColumn ) %  this.NOfRow) * 400 ) + this.yExpansionFromOrigin;
          grid.position.z = this.zExpansionFromOrigin - (Math.floor( i / this.NOFElementsInAPlane )) * 1000  ;
          d['grid'] = grid;

        },

        transform : function (layout) {
          var duration = 1000;

          TWEEN.removeAll();

          this.scene.children.forEach(function (object){
            var newPos = object.element.__data__[layout].position;
            var coords = new TWEEN.Tween(object.position)
                  .to({x: newPos.x, y: newPos.y, z: newPos.z}, duration)
                  .easing(TWEEN.Easing.Sinusoidal.InOut)
                  .start();

            var newRot = object.element.__data__[layout].rotation;
            var rotate = new TWEEN.Tween(object.rotation)
                  .to({x: newRot.x, y: newRot.y, z: newRot.z}, duration)
                  .easing(TWEEN.Easing.Sinusoidal.InOut)
                  .start();
          });

         var update = new TWEEN.Tween()
             .to({}, duration)
             .onUpdate(this.render)
             .start();
        },

        render : function () {
          this.renderer.render(this.scene, this.camera);
        },

        zoomElement:function(objt){

          this.zoomed = true;

          var px  = objt.position.x,
            py  = objt.position.y,
            pz  = objt.position.z;

          //clone the object using jquery
          var quaternion = $.extend({}, objt.quaternion);
          this.zoomedElement 
                = { 
                    element : objt,
                    px:px,
                    py:py,
                    pz:pz,
                    quaternion: quaternion
                  };

          //var objt = this.objectifiedElements[87];

          var dx = this.camera.position.x - px,
           dy = this.camera.position.y - py,
           dz = this.camera.position.z - pz;

          //distance of object from the _eye
          var dis = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2)+Math.pow(dz,2));

          var nearNess = this.getNearNessVal(dis);

          new TWEEN.Tween(objt.position)
            .to({x: px + nearNess*dx, y:py + nearNess*dy, z: pz + nearNess*dz}, 500)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .onUpdate(this.render)
            .start();

          var rotate = new TWEEN.Tween(objt.quaternion)
                .to(this.camera.quaternion, 500)
                .easing(TWEEN.Easing.Sinusoidal.InOut)
                .start();

        },

        restoreMovedElement: function(){

          this.zoomed = false;
          var objt = this.zoomedElement;

          new TWEEN.Tween(objt.element.position)
            .to({x:objt.px,y:objt.py,z:objt.pz}, 500)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .onUpdate(this.render)
            .start();

          new TWEEN.Tween(objt.element.quaternion)
            .to(objt.quaternion, 500)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .start();

        },
        
        getNearNessVal :function(dis){
          var nearNess = 0.75;
          
          if(dis<2200){
            nearNess = 0.75;
          }else if(dis<3300){
            nearNess = 0.83;
          }else if(dis<4400){
            nearNess = 0.88;
          }else if(dis>4400){
            nearNess = 0.94;
          }

          return nearNess;
        },

        addGridFront:function(){
            this.moveGrids("back");
        },

        addGridBack:function(){
            this.moveGrids("forward");
        },

        removeGridFront:function(){
            this.moveGrids("forward");
        },

        removeGridBack:function(){
            this.moveGrids("back");
        },

        moveGrids: function(moveDirection){
          if(this.zoomed){
            this.restoreMovedElement();
          } else{

          // if(!this.cameraStable){
          //   new TWEEN.Tween(this.camera.position)
          //     .to({x: 5000, y: 60, z: 0}, 300)
          //     .easing(TWEEN.Easing.Sinusoidal.InOut)
          //     .onUpdate(this.render)
          //     .start();

          //   new TWEEN.Tween(this.camera.rotation)
          //       .to({x: 0, y: 0, z: 0}, 300)
          //       .easing(TWEEN.Easing.Sinusoidal.InOut)
          //       .start();

          //   this.cameraStable=true;
          // }

          var moveBy = 1000;

          if(moveDirection === "back"){
            moveBy = -1000;
          }

          var that = this;
          $.each(this.objectifiedElements, function( index, value ) {
              new TWEEN.Tween(value.position)
                .to({z: value.position.z + moveBy}, 300)
                .start();
          });

          var update = new TWEEN.Tween()
             .to({}, 300)
             .onUpdate(this.render)
             .start();

          }

        }
    });

    return elementSystemView;

});